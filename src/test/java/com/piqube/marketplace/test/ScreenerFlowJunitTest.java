package com.piqube.marketplace.test;

import static org.junit.Assert.assertTrue;

import javax.validation.constraints.AssertFalse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.piqube.marketplace.model.Users;
import com.piqube.marketplace.service.UserService;
import com.piqube.marketplace.vo.AccountActivityVO;
import com.piqube.marketplace.vo.UserVO;

public class ScreenerFlowJunitTest {

	@Autowired
	UserService userService;
	
	public void createScreenerManager(){
		try{
			RestTemplate restTemplate = new RestTemplate();
			String url = "http://localhost:8082/admin/user";
		   UserVO userVo=new UserVO();
			ResponseEntity<?> entity =  restTemplate.postForObject(url, userVo, ResponseEntity.class);
			assertTrue(entity.getStatusCode().equals(201));
			Users user = userService.getUserByEmail(userVo.getEmail());
			String email = user.getEmail();
			String userEmail = userVo.getEmail();
			assertTrue(email.equals(userEmail));
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	public void createScreener(){
		try{
			RestTemplate restTemplate = new RestTemplate();
			String url = "http://localhost:8082/admin/subuser";
		   UserVO userVo=new UserVO();
			ResponseEntity<?> entity =  restTemplate.postForObject(url, userVo, ResponseEntity.class);
			assertTrue(entity.getStatusCode().equals(201));
			Users user = userService.getUserByEmail(userVo.getEmail());
			String email = user.getEmail();
			String userEmail = userVo.getEmail();
			assertTrue(email.equals(userEmail));
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	public void activateAndDeactivateScreener(){
		try{
			RestTemplate restTemplate = new RestTemplate();
			String url = "http://localhost:8082/admin/screener";
			AccountActivityVO accountActivityVO=new AccountActivityVO();
			ResponseEntity<?> entity =  restTemplate.postForObject(url, accountActivityVO, ResponseEntity.class);
			assertTrue(entity.getStatusCode().equals(200));
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void viewScreener(){
		try{
			RestTemplate restTemplate = new RestTemplate();
			String url = "http://localhost:8082/admin/viewScreener";
			HttpHeaders headers = new HttpHeaders();
			
					headers.setContentType(MediaType.APPLICATION_JSON);
			
					headers.set("x-auth-token", "a3ecb1bb-0e69-4781-91fb-889ca5bfe3df");
			
					System.out.println("In the method");
			
			ResponseEntity<?> entity =  restTemplate.postForObject(url, headers, ResponseEntity.class);
			assertTrue(entity.getStatusCode().equals(200));
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void screenerAvailability(){
		try{
			RestTemplate restTemplate = new RestTemplate();
			String url = "http://localhost:8082/admin/availability";
			String userName="hemalatha@infinitumlobal.com";
			
			ResponseEntity<?> entity =  restTemplate.postForObject(url, userName, ResponseEntity.class);
			assertTrue(entity.getStatusCode().equals(200));
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	

	public void screenerList(){
		try{
			RestTemplate restTemplate = new RestTemplate();
			String url = "http://localhost:8082/admin/screenerList";
			String userName="hemalatha@infinitumlobal.com";
			
			ResponseEntity<?> entity =  restTemplate.postForObject(url, userName, ResponseEntity.class);
			assertTrue(entity.getStatusCode().equals(200));
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void searchScreener(){
		try{
			RestTemplate restTemplate = new RestTemplate();
			String url = "http://localhost:8082/admin/screener";
			String email="hemalatha@infinitumlobal.com";
			String jobId="@00172";
			
			ResponseEntity<?> entity =  restTemplate.getForObject(url, ResponseEntity.class);
			assertTrue(entity.getStatusCode().equals(200));
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
}
