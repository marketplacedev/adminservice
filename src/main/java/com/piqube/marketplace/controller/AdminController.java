package com.piqube.marketplace.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.piqube.marketplace.model.Allocation;
import com.piqube.marketplace.model.Allocation.Status;
import com.piqube.marketplace.model.Authorities;
import com.piqube.marketplace.model.Authority;
import com.piqube.marketplace.model.Company;
import com.piqube.marketplace.model.CompanyType;
import com.piqube.marketplace.model.EntShortlist;
import com.piqube.marketplace.model.Job;
import com.piqube.marketplace.model.Notification;
import com.piqube.marketplace.model.NotificationPreferences;
import com.piqube.marketplace.model.ProfileData;
import com.piqube.marketplace.model.Socket;
import com.piqube.marketplace.model.Statistics;
import com.piqube.marketplace.model.UserCompany;
import com.piqube.marketplace.model.Users;
import com.piqube.marketplace.repository.AllocationRepository;
import com.piqube.marketplace.repository.JobRepository;
import com.piqube.marketplace.repository.NotificationPreferencesRepository;
import com.piqube.marketplace.repository.ProfileRepository;
import com.piqube.marketplace.repository.SocketRepository;
import com.piqube.marketplace.repository.StatisticsRepository;
import com.piqube.marketplace.repository.UserRepository;
import com.piqube.marketplace.service.AllocationService;
import com.piqube.marketplace.service.AuthoritiesService;
import com.piqube.marketplace.service.CompanyService;
import com.piqube.marketplace.service.CompanyTypeService;
import com.piqube.marketplace.service.JobDescriptionService;
import com.piqube.marketplace.service.JobService;
import com.piqube.marketplace.service.MetadataService;
import com.piqube.marketplace.service.NotificationService;
import com.piqube.marketplace.service.ProfileServiceDao;
import com.piqube.marketplace.service.StatisticsService;
import com.piqube.marketplace.service.StatisticsServiceDAO;
import com.piqube.marketplace.service.UserCompanyService;
import com.piqube.marketplace.service.UserService;
import com.piqube.marketplace.util.APIResult;
import com.piqube.marketplace.util.AdminServiceHelper;
import com.piqube.marketplace.util.Constants;
import com.piqube.marketplace.util.Helper;
import com.piqube.marketplace.vo.AccountActivityVO;
import com.piqube.marketplace.vo.AdminShortlistDetails;
import com.piqube.marketplace.vo.AdminShortlistVO;
import com.piqube.marketplace.vo.AllocationListVO;
import com.piqube.marketplace.vo.AllocationVO;
import com.piqube.marketplace.vo.AvailabilityVO;
import com.piqube.marketplace.vo.NotificationModelVO;
import com.piqube.marketplace.vo.OverAllScreenerListVO;
import com.piqube.marketplace.vo.OverrideVerdictVO;
import com.piqube.marketplace.vo.ScreenerListVO;
import com.piqube.marketplace.vo.SearchResultVO;
import com.piqube.marketplace.vo.SearchVO;
import com.piqube.marketplace.vo.SnoozeScreenerVO;
import com.piqube.marketplace.vo.UserVO;
import com.piqube.marketplace.vo.ViewScreenerVO;
import com.piqube.model.Person;

/**
 * @author girija
 */
@RestController
@EnableRedisHttpSession
@EntityScan(basePackages = "com.piqube")
@EnableJpaRepositories({ "com.piqube.marketplace.repository" })
public class AdminController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);
	Gson gson = new Gson();

	@Autowired
	CompanyService companyService;

	@Autowired
	CompanyTypeService companyTypeService;

	@Autowired
	AuthoritiesService authoritiesService;

	@Autowired
	NotificationService notificationService;

	@Autowired
	MetadataService metadataService;

	@Autowired
	StatisticsServiceDAO statisticsServiceDAO;

	@Autowired
	NotificationPreferencesRepository notificationPreferencesRepository;

	@Value("${security.user.name}")
	private String mailServiceUsername;

	@Value("${security.user.password}")
	private String mailServicePassword;

	@Value("${piqube.verification.server}")
	String verificationServer;

	@Value("${piqube.verification.port}")
	String verificationPort;

	@Value("${piqube.ui.server}")
	String UIServer;

	@Value("${piqube.ui.port}")
	String UIPort;

	@Value("${invalid.email.domains}")
	String invalidEmailDomains;
	@Autowired
	ProfileServiceDao service;

	@Autowired
	ProfileRepository profileRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	JobService jobService;

	@Autowired
	JobDescriptionService jobDescriptionService;

	@Autowired
	AllocationService allocationService;

	@Autowired
	UserService userService;

	@Autowired
	UserCompanyService userCompanyService;

	@Autowired
	StatisticsService statisticsService;

	@Autowired
	AllocationRepository allocationRepository;

	@Autowired
	JobRepository jobRepository;

	@Autowired
	StatisticsRepository statisticsRepository;

	@Autowired
	SocketRepository socketRepository;

	AdminServiceHelper adminServiceHelper = new AdminServiceHelper();

	@PreAuthorize("hasAnyAuthority('PIQUBE_ADMIN')")
	@RequestMapping(value = "/admin/job", method = RequestMethod.POST)
	public @ResponseBody @ResponseStatus(value = HttpStatus.OK) List<AdminShortlistVO> getShortlistDetails(
			@RequestBody AdminShortlistVO adminShortlistVO) throws IllegalAccessException {
		List<AdminShortlistVO> adminShortlistVOs = new ArrayList<AdminShortlistVO>();
		if (adminShortlistVO.getConsultantName() != null && adminShortlistVO.getJobId() != null) {
			Users users = userRepository.findOneByEmail(adminShortlistVO.getConsultantName());
			Long id = profileRepository.findALLJobIdAndProfileId(users.getId(), adminShortlistVO.getJobId());

			if (id != null) {
				EntShortlist entShortlist2 = profileRepository.findEntShortlistById(id);

				ProfileData profileData = service.findMongoProfileId(entShortlist2.getProfileId());

				users = userRepository.findOneById(entShortlist2.getConsultant());

				adminShortlistVO.setConsultantName(users.getEmail());
				adminShortlistVO.setJobId(entShortlist2.getJobId());
				adminShortlistVO.setProfileId(entShortlist2.getProfileId());
				adminShortlistVO.setGivenName(profileData.getName().getGiven_name());
				adminShortlistVO.setFamilyName(profileData.getName().getFamily_name());
				adminShortlistVOs.add(adminShortlistVO);

			}
		} else {
			throw new IllegalArgumentException("No JOBS AND PROFILES TO VIEW");
		}
		return adminShortlistVOs;

	}

	@PreAuthorize("hasAnyAuthority('PIQUBE_ADMIN')")
	@RequestMapping(value = "/admin/shortlist", method = RequestMethod.POST)
	public @ResponseBody @ResponseStatus(value = HttpStatus.OK) AdminShortlistDetails get(
			@RequestBody AdminShortlistVO adminShortlistVO) throws IllegalAccessException, ParseException {
		AdminShortlistDetails adminShortlistDetails = new AdminShortlistDetails();

		if (adminShortlistVO.getConsultantName() != null && adminShortlistVO.getProfileId() != null
				&& adminShortlistVO.getJobId() != null) {

			Users users = userRepository.findOneByEmail(adminShortlistVO.getConsultantName());
			Long id = profileRepository.findMaxOfEntShortlistId(adminShortlistVO.getProfileId(), users.getId(),
					adminShortlistVO.getJobId());
			if (id != null) {
				EntShortlist entShortlist = profileRepository.findEntShortlistById(id);

				SimpleDateFormat dateFormat = new SimpleDateFormat(entShortlist.getCreatedDateTime().toString());

				Date date = new Date();
				String date_s = dateFormat.format(date);
				SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
				Date tempDate = simpledateformat.parse(date_s);
				SimpleDateFormat outputDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				adminShortlistDetails.setDate(outputDateFormat.format(tempDate));

				adminShortlistDetails.setJobId(entShortlist.getJobId());
				adminShortlistDetails.setStatus(entShortlist.getStatus());
			}
		} else {
			throw new IllegalArgumentException("No JOBS AND PROFILES TO VIEW");
		}
		return adminShortlistDetails;
	}

	/**
	 * signup method for piqubedefault,companyadmin,consultantadmin
	 * 
	 * @param userVO
	 * @throws IllegalAccessException
	 */
	@PreAuthorize("hasAnyAuthority('PIQUBE_ADMIN')")
	@RequestMapping(value = "/admin", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	public void createAdmin(@RequestBody UserVO userVO) throws IllegalAccessException {

		String adminUsername = adminServiceHelper.getLoggedInUser();

		if (!(adminUsername).equals("anonymousUser")) {
			LOGGER.debug("admin user signup POST called");
			userVO.setInvalidEmailDomains(invalidEmailDomains);
			Users u = null;

			u = userService.getUserByEmail(userVO.getEmail());
			if (u != null) {
				throw new IllegalArgumentException(Constants.EMAIL_EXISTS_MSG);
			} else {
				if (userVO.getAuthority() != null && !userVO.getAuthority().isEmpty()) {
					if (userVO.getAuthority().equalsIgnoreCase(Authority.PIQUBE_ADMIN.name())
							&& adminUsername.equalsIgnoreCase("admin@piqube.com") == false)
						throw new IllegalAccessException();

					if (userVO.getAuthority().equals(Authority.COMPANY_ADMIN.name())
							|| userVO.getAuthority().equals(Authority.CONSULTANT_ADMIN.name())) {
						List<String> errors = adminServiceHelper.validateAdminSignup(userVO);

						if (errors.size() > 0)
							throw new IllegalArgumentException(StringUtils.join(errors, ","));
					}
					if (userVO.getAuthority().equals(Authority.PIQUBE_ADMIN.name())) {
						List<String> errors = adminServiceHelper.validatePiqubeSignup(userVO);

						if (errors.size() > 0)
							throw new IllegalArgumentException(StringUtils.join(errors, ","));

					}
					HashSet<Authorities> authoritiesList = new HashSet<Authorities>();
					if (userVO.getAuthority().equals(Authority.COMPANY_ADMIN.name())
							|| userVO.getAuthority().equals(Authority.CONSULTANT_ADMIN.name())
							|| userVO.getAuthority().equals(Authority.PIQUBE_ADMIN.name())) {

						Users user = new Users();
						u = userService.getUserByEmail(adminUsername);
						user.setUsername(userVO.getEmail());
						user.setFirstName(userVO.getFirstName());
						user.setLastName(userVO.getLastName());
						user.setEmail(userVO.getEmail());
						user.setCreatedBy(u);
						user.setPassword("Test");
						user.setPhoneNumber(userVO.getPhoneNumber());
						if (!(userVO.getAuthority()).equals(Authority.PIQUBE_ADMIN.name())) {
							UserCompany existingCompany = userCompanyService.findByCompanyNameAndCompanyLocation(
									userVO.getCompanyName(), userVO.getCompanyLocation());
							if (null != existingCompany)
								throw new IllegalArgumentException("This company is already registerd with us");

							CompanyType companyType = companyTypeService.findOneByCompanySize(userVO.getCompanySize());

							UserCompany userCompany = new UserCompany();
							userCompany.setCompanyName(userVO.getCompanyName());
							userCompany.setCompanyLocation(userVO.getCompanyLocation());
							userCompany.setCompanyType(companyType);
							userCompany.setCompanyCategory(userVO.getCompanyCategory());
							userCompany.setAccountLocked(true);

							Company comp = companyService.findByCompanyName(userVO.getCompanyName());
							if (null != comp) {
								userCompany.setIsFreeText(false);
								userCompany.setMasterCompany(comp);
							} else
								userCompany.setIsFreeText(true);

							UserCompany newCompany = userCompanyService.createCompany(userCompany);
							user.setCompany(newCompany);
						} else {
							u = userService.getUserByEmail(adminUsername);

							user.setCompany(u.getCompany());

						}

						Authorities authority = new Authorities();

						authority.setUser(user);
						authority.setAuthority(userVO.getAuthority());
						authoritiesList.add(authority);
						Authorities subuser_authority = new Authorities();

						if (!(userVO.getAuthority()).equals(Authority.PIQUBE_ADMIN.name())) {
							if (authority.getAuthority().equals("CONSULTANT_ADMIN")) {
								subuser_authority = new Authorities();
								subuser_authority.setAuthority("CONSULTANT");
							}
							if (authority.getAuthority().equals("COMPANY_ADMIN")) {

								subuser_authority = new Authorities();
								subuser_authority.setAuthority("RECRUITER");
							}
							authoritiesList.add(subuser_authority);

						}
						user.setAuthorities(authoritiesList);
						Users createdUser = userService.createUser(user);
						if (!(userVO.getAuthority()).equals(Authority.PIQUBE_ADMIN.name())) {
							subuser_authority.setUser(createdUser);
							authoritiesService.saveAuthority(subuser_authority);
						}

						if (authority.getAuthority().equals("CONSULTANT_ADMIN")) {
							Statistics stats = new Statistics();
							stats.setAuthority(Authority.CONSULTANT_ADMIN.name());
							stats.setEnabled(0);
							stats.setUserId(user.getId());
							stats.setJobCapacity(3);
							stats.setPositionsCapacity(10);
							stats.setJobsCurrent(0);
							stats.setPositionsCurrent(0);
							stats.setScore(Double.valueOf(250));
							stats.setPreference("{\"IND any\":1,\"IND \":5}");
							statisticsServiceDAO.createStatistics(stats);
						}

						NotificationPreferences preferences = new NotificationPreferences();
						preferences.setUsername(userVO.getEmail());
						preferences.setCreatedAt(new Date());
						notificationPreferencesRepository.saveAndFlush(preferences);

						sendEMail(createdUser, null, Notification.Template.ADMIN_EMAIL_VERIFICATION.toString(),
								Notification.Template.ADMIN_EMAIL_VERIFICATION.getTemplateCode());
						sendEMail(createdUser, null, Notification.Template.ADMIN_REGISTERED.toString(),
								Notification.Template.ADMIN_REGISTERED.getTemplateCode());

						LOGGER.debug("Signup done. Email verification needs to be done for :{}",
								createdUser.getUsername());

					} else
						throw new IllegalArgumentException(Constants.AUTHORITY_NOT_FOUND);

				} else {
					throw new IllegalArgumentException(Constants.AUTHORITY_NOT_FOUND);
				}
			}
		} else
			throw new IllegalArgumentException(Constants.ANONYMOUS_USER);
	}

	/**
	 * create companymanager,consultantmanager,screenermanager,screener and send
	 * mail to them to request to update details
	 * 
	 * @param userVO
	 */
	@PreAuthorize("hasAnyAuthority('PIQUBE_ADMIN','SCREENER_MANAGER')")
	@RequestMapping(value = "/admin/user", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	public ResponseEntity<String> createUsers(@RequestBody UserVO userVO) {
		String adminUsername = adminServiceHelper.getLoggedInUser();

		LOGGER.debug("admin user POST is called");
		Users adminUser = userService.getUserByEmail(adminUsername);

		if (userVO.getEmail() != null && !"".equals(userVO.getEmail())) {
			boolean validity = adminServiceHelper.isValidEmailAddress(userVO.getEmail());
			if (!validity)
				return new ResponseEntity<>("Please enter a valid Email", HttpStatus.BAD_REQUEST);
			;

		}

		List<String> errors = adminServiceHelper.validatePiqubeSignup(userVO);

		if (errors.size() > 0)
			return new ResponseEntity<>(StringUtils.join(errors, ","), HttpStatus.BAD_REQUEST);

		if (userVO.getEmail() != null && !"".equals(userVO.getEmail())) {
			Users u = userService.getUserByEmail(userVO.getEmail());

			if (u != null)
				return new ResponseEntity<>(Constants.EMAIL_EXISTS_MSG, HttpStatus.CONFLICT);
			else {
				Users user = new Users();
				user.setUsername(userVO.getEmail());
				user.setEmail(userVO.getEmail());
				user.setCompany(adminUser.getCompany());
				user.setCreatedBy(adminUser);
				user.setPassword("Test");
				if (userVO.getPhoneNumber() != null && !"".equals(userVO.getPhoneNumber()))
					user.setPhoneNumber(userVO.getPhoneNumber());
				if (userVO.getFirstName() != null && !"".equals(userVO.getFirstName()))
					user.setFirstName(userVO.getFirstName());
				if (userVO.getLastName() != null && !"".equals(userVO.getLastName()))
					user.setLastName(userVO.getLastName());

				NotificationPreferences preferences = new NotificationPreferences();
				preferences.setUsername(userVO.getEmail());
				preferences.setCreatedAt(new Date());
				notificationPreferencesRepository.saveAndFlush(preferences);

				if (adminUser.hasAuthority(Authority.PIQUBE_ADMIN)
						|| (adminUser.hasAuthority(Authority.SCREENER_MANAGER))) {

					if (userVO.getAuthority().equals("COMPANY_MANAGER")
							|| (userVO.getAuthority().equals("CONSULTANT_MANAGER")
									|| (userVO.getAuthority().equals("SCREENER_MANAGER")))) {
						Users createdUser = userService.createUser(user);
						Authorities auth = new Authorities();
						auth.setUser(user);
						auth.setAuthority(userVO.getAuthority());
						authoritiesService.saveAuthority(auth);
						sendEMail(createdUser, null, Notification.Template.ADMIN_EMAIL_VERIFICATION.toString(),
								Notification.Template.ADMIN_EMAIL_VERIFICATION.getTemplateCode());

					}

					if (userVO.getAuthority().equals("SCREENER")) {
						Users createdUser = userService.createUser(user);
						Authorities auth = new Authorities();
						auth.setUser(user);
						auth.setAuthority(userVO.getAuthority());
						authoritiesService.saveAuthority(auth);
						Statistics stats = new Statistics();

						// create entry in allocation table
						stats.setAuthority(Authority.SCREENER.name());
						stats.setEnabled(1);
						stats.setUserId(user.getId());
						stats.setJobCapacity(3);
						stats.setPositionsCapacity(10);
						stats.setJobsCurrent(0);
						stats.setPositionsCurrent(0);
						stats.setScore(Double.valueOf(250));
						if (userVO.getPreferences() != null && !"".equals(userVO.getPreferences())) {
							Map<String, Integer> result = new HashMap<>();
							if (userVO.getPreferences().size() > 0) {
								result = adminServiceHelper.setPreferences(userVO.getPreferences());
							} else {
								result.put("IND any", 1);
							}
							stats.setPreference(new Gson().toJson(result));
						} else {
							return new ResponseEntity<>("Please enter the preferences", HttpStatus.BAD_REQUEST);

						}
						statisticsServiceDAO.createStatistics(stats);

						sendEMail(createdUser, null, Notification.Template.ADMIN_EMAIL_VERIFICATION.toString(),
								Notification.Template.ADMIN_EMAIL_VERIFICATION.getTemplateCode());
						allocationService.doShorlistAllocations();
					}
				}

			}
		}

		LOGGER.debug("exiting subuser POST");
		return null;

	}

	/**
	 * create recruiters/consultants accounts by piqube admin representative and
	 * send mail to them to request to update details
	 * 
	 * @param userVO
	 */
	@PreAuthorize("hasAnyAuthority('PIQUBE_ADMIN','CONSULTANT_MANAGER','COMPANY_MANAGER')")
	@RequestMapping(value = "/admin/subuser", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	public void createSubuserAdmin(@RequestBody UserVO userVO) {
		LOGGER.debug("admin sub user POST is called");
		List<String> errors = adminServiceHelper.validateAdminSubUserSignup(userVO);
		if (errors.size() > 0)
			throw new IllegalArgumentException(StringUtils.join(errors, ","));
		if (userVO.getCompanyName() != null && userVO.getCompanyLocation() != null) {
			UserCompany userCompany = userCompanyService.findCompanyName(userVO.getCompanyName(),
					userVO.getCompanyLocation());

			if (userCompany != null) {
				Users users = userService.findAdminByCompanyId(userCompany.getId());

				Users adminUser = userService.getUserByEmail(users.getEmail());

				if (userVO.getEmail() != null && !userVO.getEmail().isEmpty()) {
					boolean validity = adminServiceHelper.isValidEmailAddress(userVO.getEmail());
					if (!validity)
						throw new IllegalArgumentException("Please enter a valid Email");

					if (adminUser.hasAuthority(Authority.COMPANY_ADMIN)) {
						List<String> invalidEmailDomainList = Arrays.asList(invalidEmailDomains.split("\\s*,\\s*"));
						for (String invalidDomain : invalidEmailDomainList) {
							if (userVO.getEmail().contains(invalidDomain))
								throw new IllegalArgumentException("Please enter a Corporate Email address");
						}
					}

				} else {
					throw new IllegalArgumentException(Constants.EMAIL);
				}

				if (userVO.getEmail() != null && !"".equals(userVO.getEmail())) {
					Users u = userService.getUserByEmail(userVO.getEmail());

					if (u != null)
						throw new IllegalArgumentException(Constants.EMAIL_EXISTS_MSG);
					else {
						Users user = new Users();
						user.setUsername(userVO.getEmail());
						user.setEmail(userVO.getEmail());
						user.setCompany(adminUser.getCompany());
						user.setCreatedBy(adminUser);
						user.setPassword("Test");
						if (userVO.getPhoneNumber() != null && !"".equals(userVO.getPhoneNumber()))
							user.setPhoneNumber(userVO.getPhoneNumber());
						if (userVO.getFirstName() != null && !"".equals(userVO.getFirstName()))
							user.setFirstName(userVO.getFirstName());
						if (userVO.getLastName() != null && !"".equals(userVO.getLastName()))
							user.setLastName(userVO.getLastName());

						NotificationPreferences preferences = new NotificationPreferences();
						preferences.setUsername(userVO.getEmail());
						preferences.setCreatedAt(new Date());
						notificationPreferencesRepository.saveAndFlush(preferences);

						Users createdUser = userService.createUser(user);
						Authorities auth = new Authorities();
						auth.setUser(user);
						auth.setAuthority(userVO.getAuthority());
						authoritiesService.saveAuthority(auth);
						sendEMail(createdUser, null, Notification.Template.ADMIN_EMAIL_VERIFICATION.toString(),
								Notification.Template.ADMIN_EMAIL_VERIFICATION.getTemplateCode());
						LOGGER.debug("Request for subuser {} signup is sent", userVO.getEmail());

						LOGGER.debug("exiting subuser POST");

					}
				}

			} else {
				throw new IllegalArgumentException(Constants.COMPANY_REGISTER);
			}
		} else {
			throw new IllegalArgumentException(Constants.COMPANY_NAME);
		}

	}

	@RequestMapping(value = "/admin/subuser/create-account", method = RequestMethod.GET)
	public RedirectView createSubuser(@RequestParam(value = "key", required = false) String key,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "firstName", required = false) String firstName,
			@RequestParam(value = "lastName", required = false) String lastName,
			@RequestParam(value = "companyName", required = false) String companyName) {

		LOGGER.debug("subuser create account GET is called...");
		Users user = userService.findUserByUUID(key);
		RedirectView redirectView = new RedirectView();
		if (null != user) {
			user.setEmailVerified(true);
			userService.updateUser(user);
			LOGGER.debug("SubUser email is successfully verified for user with uuid:{}", key);

			redirectView.setUrl("http://" + UIServer + ":" + UIPort + "/screener-onboard?key=" + key + "&firstName="
					+ firstName + "&lastName=" + lastName + "&email=" + email + "&companyName=" + companyName);

			LOGGER.debug("redirecting to login...");

			return redirectView;
		} else {
			LOGGER.debug("create account key is invalid -{}", key);
			throw new IllegalArgumentException("create account key is invalid");
		}

	}

	/**
	 * method used by subuser to do signup
	 * 
	 * @param userVO
	 */
	@RequestMapping(value = "/admin/subuser/{key}", method = RequestMethod.PUT)
	@ResponseStatus(value = HttpStatus.OK)
	public void createSubUser(@PathVariable String key, @Valid @RequestBody UserVO userVO)
			throws IllegalAccessException {

		LOGGER.debug("subuser PUT is called...");
		List<String> errors = adminServiceHelper.validateSubUserSignup(userVO);
		if (errors.size() > 0)
			throw new IllegalArgumentException(StringUtils.join(errors, ","));

		LOGGER.debug("Subuser with uuid {} is going to edit his account.", key);
		if (null != key) {
			Users user = userService.findUserByUUID(key);
			Statistics stats = new Statistics();

			if (user != null) {
				Users admin = user.getCreatedBy();
				if (userVO.getFirstName() != null && !"".equals(userVO.getFirstName()))
					user.setFirstName(userVO.getFirstName());
				if (userVO.getFirstName() != null && !"".equals(userVO.getFirstName()))
					user.setLastName(userVO.getLastName());

				user.setPhoneNumber(userVO.getPhoneNumber());
				user.setPassword(new BCryptPasswordEncoder().encode(userVO.getPassword()));
				user.setAccountEnabled(true);
				user.setEmailVerified(true);
				userService.updateUser(user);
				if (admin.hasAuthority(Authority.CONSULTANT_ADMIN)) {

					sendEMail(user, null, Notification.Template.CONSULTANCY_STATEMENT_OF_WORK.toString(),
							Notification.Template.CONSULTANCY_STATEMENT_OF_WORK.getTemplateCode());
					// create entry in allocation table
					stats.setAuthority(Authority.CONSULTANT.name());
					LOGGER.debug("=====company lock status=====" + user.getCompany().isAccountLocked());
					if (user.getCompany().isAccountLocked())
						stats.setEnabled(0);
					else
						stats.setEnabled(1);
					stats.setUserId(user.getId());
					stats.setJobCapacity(3);
					stats.setPositionsCapacity(10);
					stats.setJobsCurrent(0);
					stats.setPositionsCurrent(0);
					stats.setScore(Double.valueOf(250));
					if (userVO.getPreferences() != null && !"".equals(userVO.getPreferences())) {
						Map<String, Integer> result = adminServiceHelper.setPreferences(userVO.getPreferences());
						stats.setPreference(new Gson().toJson(result));
					} else
						throw new IllegalArgumentException("Please enter the preferences");
					statisticsServiceDAO.createStatistics(stats);
					allocationService.doJobAllocations();

				}
				if (user.hasAuthority(Authority.SCREENER)) {
					sendEMail(user, null, Notification.Template.SCREENER_STATEMENT_OF_WORK.toString(),
							Notification.Template.SCREENER_STATEMENT_OF_WORK.getTemplateCode());
				}

				NotificationPreferences notificationPreferences = notificationPreferencesRepository
						.findByUsername(user.getUsername());
				if (null == notificationPreferences) {
					LOGGER.info("====Notification preferences not found=====");
					NotificationPreferences preferences = new NotificationPreferences();
					preferences.setUsername(user.getUsername());
					preferences.setCreatedAt(new Date());
					notificationPreferencesRepository.saveAndFlush(preferences);
				}
				LOGGER.debug("exiting subuser put");

			} else
				throw new IllegalAccessException(Constants.RECORD_NOT_FOUND_MSG);

		} else
			throw new IllegalArgumentException("Please send the key from signup url.");

	}

	/**
	 * Assign work to screener
	 *
	 */
	@PreAuthorize("hasAnyAuthority('PIQUBE_ADMIN','SCREENER_MANAGER')")
	@RequestMapping(value = "/admin/allocation", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void assignJob(AllocationVO allocationVO) throws IllegalAccessException {
		LOGGER.debug("Assign work to screener is called...");

		Users user = userService.getUserByEmail(allocationVO.getEmail());
		if (user != null) {

			Statistics stats = statisticsService.getStatisticsById(user.getId());
			Allocation alloc = allocationRepository.findAlloocationByUserAndShortlist(user,
					UUID.fromString(allocationVO.getShortlistId()));
			if (alloc != null) {
				throw new IllegalArgumentException("Job Assigned Already");
			} else {
				// checking current job count
				if (stats.getJobsCurrent() < 10) {
					List<EntShortlist> shortlist = profileRepository
							.findByShortlistId(UUID.fromString(allocationVO.getShortlistId()));
					if (shortlist != null) {
						Allocation allocation = allocationService
								.assignAllocation(UUID.fromString(allocationVO.getShortlistId()), user);
						if (allocation != null) {
							// increment jobs current in stats
							Integer statsJobCount = 0;
							if (stats.getJobsCurrent() != null)
								statsJobCount = stats.getJobsCurrent();
							stats.setJobsCurrent(statsJobCount + 1);
							statisticsRepository.saveAndFlush(stats);

							sendEMail(user, null, Notification.Template.WORK_ASSIGNED.toString(),
									Notification.Template.WORK_ASSIGNED.getTemplateCode());

						}
					} else {
						throw new IllegalArgumentException("Please provide correct shortlistId");
					}
				} else {
					throw new IllegalArgumentException("Job Capacity Exceeds");
				}
			}
		} else {

			throw new IllegalArgumentException("Please provide correct email");
		}
	}

	/**
	 * Remove work from screener
	 *
	 */
	@PreAuthorize("hasAnyAuthority('PIQUBE_ADMIN','SCREENER_MANAGER')")
	@RequestMapping(value = "/admin/allocation/{email:.+}/{shortlistId}", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.OK)
	public void removeWork(@PathVariable String email, @PathVariable String shortlistId) throws IllegalAccessException {
		LOGGER.debug("Remove work from screener is called...");

		Users user = userService.getUserByEmail(email);
		if (user != null) {
			Statistics stats = statisticsService.getStatisticsById(user.getId());

			// entry in allocation table
			if (shortlistId != null) {
				Allocation allocation = allocationService.findCurrentAllocationForUser(shortlistId, user);
				if (allocation != null) {
					Allocation alloc = allocationService.abortAllocation(allocation);
				} else {
					throw new IllegalArgumentException("Please provide shortlistId");
				}
				// decrement jobs current in stats
				Integer statsJobCount = 0;
				if (stats.getJobsCurrent() != null && stats.getJobsCurrent() != 0)
					statsJobCount = stats.getJobsCurrent();
				stats.setJobsCurrent(statsJobCount - 1);
				statisticsRepository.saveAndFlush(stats);
			} else {
				throw new IllegalArgumentException("Please provide shortlistId");
			}
		} else {
			throw new IllegalArgumentException("Please provide correct email");
		}
	}

	/**
	 * Activate,Deactivate screener
	 *
	 */
	@PreAuthorize("hasAnyAuthority('PIQUBE_ADMIN','SCREENER_MANAGER')")
	@RequestMapping(value = "/admin/screener", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void activateScreener(@RequestBody AccountActivityVO accountActivityVO) throws IllegalAccessException {
		if(accountActivityVO!=null){
		if (accountActivityVO.getAccountActivity().equals(true)) {
			LOGGER.debug("Activate screener is called...");
			if (accountActivityVO.getEmail() != null) {
				Users user = userService.getUserByEmail(accountActivityVO.getEmail());
				if (user != null) {
					// enable 1 in stats
					Statistics statistics = statisticsService.getStatisticsByUserId(user.getId());
					statistics.setEnabled(1);
					statisticsRepository.saveAndFlush(statistics);
					user.setAccountEnabled(true);
					user.setAccountLocked(false);
					userRepository.save(user);
					// send email

					sendEMail(user, null, Notification.Template.ACTIVATION_NOTIFICATION.toString(),
							Notification.Template.ACTIVATION_NOTIFICATION.getTemplateCode());

					// call shortlist allocation
					allocationService.doShorlistAllocations();
				} else {
					throw new IllegalArgumentException("Please provide correct email");
				}
			} else {
				throw new IllegalArgumentException("Please provide correct email");
			}
		} else {
			LOGGER.debug("Deactivate screener is called...");
			if (accountActivityVO.getEmail() != null) {
				Users user = userService.getUserByEmail(accountActivityVO.getEmail());
				if (user != null) {
					// enable 0 in stats
					Statistics statistics = statisticsService.getStatisticsByUserId(user.getId());
					statistics.setEnabled(0);
					statisticsRepository.saveAndFlush(statistics);

					List<Allocation> allocation = allocationRepository.findAllocationForUserByStatus(user,
							adminServiceHelper.getAllocationStatuses());
					// entry in allocation table
					if (allocation != null) {
						for (Allocation allocation2 : allocation) {

							Allocation alloc = allocationService.abortAllocation(allocation2);
						}
					}
					user.setAccountEnabled(false);
					user.setAccountLocked(true);
					userRepository.save(user);
					// call shortlist allocation
					allocationService.doShorlistAllocations();
				} else {
					throw new IllegalArgumentException("Please provide correct email");
				}

			} else {
				throw new IllegalArgumentException("Please provide correct email");
			}
		}
		}else{
			throw new IllegalArgumentException("Please provide Request data");
		}
	}

	/**
	 * Remove screener
	 *
	 */
	@PreAuthorize("hasAnyAuthority('PIQUBE_ADMIN','SCREENER_MANAGER')")
	@RequestMapping(value = "/admin/screener/{email:.+}", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.OK)
	public void removeScreener(@PathVariable String email) throws IllegalAccessException {
		LOGGER.debug("remove screener is called...");
		if (email != null) {
			String adminUsername = adminServiceHelper.getLoggedInUser();
			Users user = userService.getUserByEmail(email);
			Users admin = userService.getUserByEmail(adminUsername);
			if (user != null) {
				// entry in allocation table
				List<Allocation> allocation = allocationRepository.findAllocationForUserByStatus(user,
						adminServiceHelper.getAllocationStatuses());
				if (allocation != null) {
					for (Allocation allocation2 : allocation) {
						Allocation alloc = allocationService.abortAllocation(allocation2);

					}
				}
				// call shortlist allocation
				allocationService.doShorlistAllocations();

				// enable 1 in stats
				Statistics stats = statisticsService.getStatisticsById(user.getId());
				stats.setEnabled(0);
				stats.setJobsCurrent(0);
				statisticsRepository.save(stats);
				user.setAccountLocked(true);
				userRepository.save(user);
				// remove user

				String status = userService.removeUser(user, admin);
				
			} else {
				throw new IllegalArgumentException("Please provide correct email");
			}
		} else {
			throw new IllegalArgumentException("Please provide correct email");
		}

	}

	/**
	 * Active shortlist
	 *
	 */
	@PreAuthorize("hasAnyAuthority('PIQUBE_ADMIN','SCREENER_MANAGER')")
	@RequestMapping(value = "/admin/activeShortlist", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public List<AllocationListVO> activeShortlist() throws IllegalAccessException {
		List<Allocation> allocation = allocationRepository.findActiveScreeners();
		List<AllocationListVO> allocList = new ArrayList<AllocationListVO>();
		for (Allocation allocation2 : allocation) {
			Job job = jobService.findById(allocation2.getJob());
			AllocationListVO allocationListVO = new AllocationListVO();
			allocationListVO.setAllocationId(allocation2.getAllocationId().toString());
			allocationListVO.setShortlistId(allocation2.getShortlistId().toString());
			allocationListVO.setStatus(allocation2.getStatus());
			allocationListVO.setUserId(allocation2.getUser().getEmail());
			allocList.add(allocationListVO);
		}
		return allocList;
	}

	// /**
	// * Search screener
	// *
	// */
	// @RequestMapping(value = "/admin/screener", method = RequestMethod.GET,
	// headers = "Accept=*/*", produces = "application/json")
	// @ResponseBody
	// public List<Users> getSearch(@RequestParam String email) throws Exception
	// {
	//
	// LOGGER.debug("search is called...");
	// List<Users> list = new ArrayList<>();
	// List<Users> users =
	// userService.findUsersByAuthority(Authority.SCREENER.name());
	// List<Long> userIds = new ArrayList<>();
	// for (Users user : users) {
	// userIds.add(user.getId());
	// }
	//
	// // if (query.length() > 6) {
	// list = userService.findUsersStartingWith(email.toLowerCase() + "%",
	// userIds);
	//
	// /*
	// * if (list.size() > 0) return list; else { for (int i = query.length();
	// * i > 6; i--) { query = query.substring(0, query.length() - 1); list =
	// * userService.findUsersStartingWith(query.toLowerCase() + "%",userIds);
	// * if (list.size() > 0) break; } } }
	// */
	// return list;
	// }

	/**
	 * Search screener Result
	 *
	 */

	@RequestMapping(value = "/admin/screener/{username:.+}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public SearchVO getSearchResults(@PathVariable String username) throws Exception {
		SearchVO searchVO = new SearchVO();
		LOGGER.debug("search screener  is called...");
		Users user = userRepository.findOneByEmail(username);
		EntShortlist entShortlist = null;
		if (user != null) {
			Date maxCreatedDate = allocationRepository.getMaxCreatedAt(user, "CLOSED");
			if (maxCreatedDate != null) {
				String shortListId = allocationRepository.getShortlistIdByLastDate(maxCreatedDate);
				if (shortListId != null) {
					entShortlist = profileRepository.findCurrentStatus(user.getId(), UUID.fromString(shortListId));
					searchVO.setEmail(user.getEmail());
					searchVO.setPhoneNumber(user.getPhoneNumber());
					searchVO.setLastScreenedDate(maxCreatedDate);
					if (entShortlist.getScreenerResponse().equals(0)) {
						searchVO.setLastScreenedStatus("APPROVED");
					} else {
						searchVO.setLastScreenedStatus("REJECTED");
					}
				} else {
					throw new IllegalArgumentException("Screener not shortlist any profile till now");
				}

			} else {
				throw new IllegalArgumentException("Screener not shortlist any profile till now");
			}

		} else {
			throw new IllegalArgumentException("Please provide correct email");
		}
		return searchVO;
	}

	/**
	 * Users List
	 *
	 */
	@PreAuthorize("hasAnyAuthority('PIQUBE_ADMIN')")
	@RequestMapping(value = "/admin/getusers", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody @ResponseStatus(value = HttpStatus.OK) String getAllUsersByAuthority(
			@RequestParam("authority") String authority) throws IllegalAccessException {

		LOGGER.debug("getAllUsersByAuthority is called" + authority);

		List<Users> users = userService.findAllUsersByAuthority(authority);
		if (users.size() > 0) {
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
			return gson.toJson(users);
		}
		throw new IllegalArgumentException("No Users Found!");
	}

	/* Snooze screener funtionality */

	@PreAuthorize("hasAnyAuthority('PIQUBE_ADMIN','SCREENER_MANAGER','SCREENER')")
	@RequestMapping(value = "/admin/snoozeScreener", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody ResponseEntity<?> snoozeScreener(Principal user, @RequestBody String snoozeData)
			throws IllegalAccessException {

		APIResult result = new APIResult();
		SnoozeScreenerVO snoozeObject = gson.fromJson(snoozeData, SnoozeScreenerVO.class);
		GrantedAuthority authority = ((UserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal()).getAuthorities().iterator().next();
		if (authority.getAuthority().equals("SCREENER")) {

			if (!user.getName().equals(snoozeObject.getScreenerEmail())) {
				result.setData("You cannot snooze this screener");
				return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
			}
		}

		if (snoozeObject == null) {
			return new ResponseEntity<>(gson.toJson(result), HttpStatus.NO_CONTENT);
		} else {

			Users screenerUser = userRepository.findOneByEmail(snoozeObject.getScreenerEmail());
			Statistics screener = statisticsRepository.findByUserId(screenerUser.getId());

			DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			Date start = null;
			Date end = null;
			try {
				start = df.parse(snoozeObject.getSnoozeStartTime());
				end = df.parse(snoozeObject.getSnoozeEndTime());
				LocalDate localDate = new LocalDate();
				Date dNow = localDate.toDate();

				if (start.equals(dNow)) {
					screener.setEnabled(0);
					if (screenerUser != null) {
						// Deallocating all current jobs
						List<Allocation> allAllocations = allocationRepository
								.findCurrentAllocationOfScreeners(screenerUser);
						for (Allocation allocation : allAllocations) {
							Allocation alloc = allocationService.abortAllocation(allocation);
						}
					}
				}

			} catch (ParseException e) {
				result.setData("Something went wrong while parsing!");
				return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);
			}

			if (snoozeObject.getScreenerEmail() != null) {

				screener.setSnoozeStartTime(start);
				screener.setSnoozeEndTime(end);
				statisticsRepository.save(screener);

			}

		}

		result.setData("Snooze Time updated!");
		return new ResponseEntity<>(gson.toJson(result), HttpStatus.OK);

	}

	public void sendEMail(Users createdUser, Users admin, String templateType, int templateCode) {

		NotificationModelVO notificationModel = new NotificationModelVO();
		Notification notification = new Notification();
		notificationModel.setUuid(createdUser.getuUID());
		notificationModel.setEmailVerificationServer(verificationServer.trim());
		notificationModel.setEmailVerificationServerPort(verificationPort.trim());

		if (templateType.equals(Notification.Template.ADMIN_REGISTERED.name())) {
			if (createdUser.getFirstName() != null && !"".equals(createdUser.getFirstName()))
				notificationModel.setFirstName(createdUser.getFirstName());
			if (createdUser.getLastName() != null && !"".equals(createdUser.getLastName()))
				notificationModel.setLastName(createdUser.getLastName());
			if (createdUser.getEmail() != null && !"".equals(createdUser.getEmail()))
				notificationModel.setEmail(createdUser.getEmail());
			if (createdUser.getCompany() != null && !"".equals(createdUser.getCompany()))
				notificationModel.setCompanyName(createdUser.getCompany().getCompanyName());
			if (createdUser.getPhoneNumber() != null && !"".equals(createdUser.getPhoneNumber()))
				notificationModel.setPhoneNumber(createdUser.getPhoneNumber());

			Set<Authorities> authorities = authoritiesService.findByUsername(createdUser.getUsername());
			for (Authorities authority : authorities) {

				if (authority.getAuthority().equals(Authority.COMPANY_ADMIN.name()))
					notificationModel.setAuthority(Authority.COMPANY_ADMIN.name());
				else if (authority.getAuthority().equals(Authority.CONSULTANT_ADMIN.name()))
					notificationModel.setAuthority(Authority.CONSULTANT_ADMIN.name());
			}
		} else {
			try {
				if (createdUser.getFirstName() != null && !"".equals(createdUser.getFirstName()))
					notificationModel.setFirstName(URLEncoder.encode(createdUser.getFirstName(), "UTF-8"));
				if (createdUser.getLastName() != null && !"".equals(createdUser.getLastName()))
					notificationModel.setLastName(URLEncoder.encode(createdUser.getLastName(), "UTF-8"));
				if (createdUser.getEmail() != null && !"".equals(createdUser.getEmail()))
					notificationModel.setEmail(URLEncoder.encode(createdUser.getEmail(), "UTF-8"));
				if (createdUser.getCompany() != null && !"".equals(createdUser.getCompany()))
					notificationModel
							.setCompanyName(URLEncoder.encode(createdUser.getCompany().getCompanyName(), "UTF-8"));
				if (createdUser.getPhoneNumber() != null && !"".equals(createdUser.getPhoneNumber()))
					notificationModel.setPhoneNumber(URLEncoder.encode(createdUser.getPhoneNumber(), "UTF-8"));

				if (null != admin) {
					if (admin.getFirstName() != null && !"".equals(admin.getFirstName()))
						notificationModel.setAdminFirstName(URLEncoder.encode(admin.getFirstName(), "UTF-8"));
					if (admin.getLastName() != null && !"".equals(admin.getLastName()))
						notificationModel.setAdminLastName(URLEncoder.encode(admin.getLastName(), "UTF-8"));
					GrantedAuthority authority = ((UserDetails) SecurityContextHolder.getContext().getAuthentication()
							.getPrincipal()).getAuthorities().iterator().next();
					notificationModel.setAuthority(authority.getAuthority());
				}
				List<String> recipients = new ArrayList<String>();
				recipients.add(createdUser.getEmail());
				notification.setRecipients(recipients);

			} catch (UnsupportedEncodingException e) {
				LOGGER.error("error in encoding");
			}

		}
		String model = Helper.convertObjectToJson(notificationModel);

		notification.setEmail(createdUser.getEmail());
		notification.setTemplate(templateCode);
		notification.setTemplateType(templateType);
		notification.setModel(model);

		/*
		 * RestTemplate template = new
		 * TestRestTemplate(mailServiceUsername,mailServicePassword);
		 * template.postForObject("http://"+notificationServer+":"+
		 * notificationServerPort+"/Notification", notification,
		 * Notification.class);
		 */
		notificationService.sendNotification(notification);
	}

	// @PreAuthorize("hasAnyAuthority('SCREENER_MANAGER','PIQUBE_ADMIN')")
	// @RequestMapping(value = "/admin/screenerList", method =
	// RequestMethod.GET)
	// @ResponseStatus(value = HttpStatus.OK)
	// public @ResponseBody String common() throws IllegalAccessException {
	//
	// LOGGER.debug("screenerlist is called" + "screenerlist");
	// String username = adminServiceHelper.getLoggedInUser();
	// Users user = userService.getUserByEmail(username);
	//
	// List<Users> usersList =
	// userService.findAllActiveUsersOfAdminByCreatedBy(user);
	//
	// List<OverAllScreenerListVO> overAllScreenerListVO = new
	// ArrayList<OverAllScreenerListVO>();
	// List<Socket> offlineScreeners = null;
	// List<Socket> onlineScreeners = null;
	// DateTime febDate = new DateTime("2016-02-22T01:00:00.000+08:00");
	// for (Users users : usersList) {
	// offlineScreeners =
	// socketRepository.findOfflineScreeners(users.getEmail());
	// System.out.println("offlineScreeners=" + offlineScreeners.size());
	// onlineScreeners = socketRepository.findByUsername(users.getEmail());
	// System.out.println("onlineScreeners=" + onlineScreeners.size());
	// ScreenerListVO screenerListVO = new ScreenerListVO();
	// OverAllScreenerListVO overAllScreenerVO = new OverAllScreenerListVO();
	// if (onlineScreeners.size() > 0) {
	// for (Socket socket : onlineScreeners) {
	//
	// screenerListVO.setScreenerName(socket.getUsername());
	// Users userName = userService.getUserByEmail(socket.getUsername());
	//
	// int jobInQueue =
	// allocationRepository.findFinalCountOfJobsByStatus(userName.getId(),
	// Status.NEW.name(), febDate.toDate());
	// screenerListVO.setJobsInQueue(jobInQueue);
	// screenerListVO.setAvailability(true);
	// overAllScreenerVO.setScreenerListVO(screenerListVO);
	// }
	// } else {
	// for (Socket socket : offlineScreeners) {
	//
	// String socketUsername=socket.getUsername();
	// String split[] = StringUtils.split(socketUsername,"+");
	// String part1 = split[0];
	// screenerListVO.setScreenerName(part1);
	// Users userName = userService.getUserByEmail(part1);
	// int jobInQueue =
	// allocationRepository.findFinalCountOfJobsByStatus(userName.getId(),
	// Status.NEW.name(), febDate.toDate());
	// screenerListVO.setJobsInQueue(jobInQueue);
	// screenerListVO.setAvailability(false);
	// screenerListVO.setLastSeen(socket.getCreatedAt());
	// overAllScreenerVO.setScreenerListVO(screenerListVO);
	//
	// }
	//
	// }
	//
	// overAllScreenerListVO.add(overAllScreenerVO);
	//
	// }
	// return gson.toJson(overAllScreenerListVO);
	// }

	/**
	 * View screener Statistics
	 *
	 */
	@PreAuthorize("hasAnyAuthority('SCREENER_MANAGER','PIQUBE_ADMIN')")
	@RequestMapping(value = "/admin/viewScreener", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody ResponseEntity<?> getScreenerList() {

		LOGGER.debug("screenerlist is called" + "screenerlist");
		String username = adminServiceHelper.getLoggedInUser();
		Users user = userService.getUserByEmail(username);
		APIResult result = new APIResult();

		// Find All sub screeners
		List<Users> usersList = userService.findAllActiveUsersOfAdminByCreatedBy(user);

		List<OverAllScreenerListVO> overAllScreenerListVO = new ArrayList<OverAllScreenerListVO>();

		// Set screener statistics
				if (usersList.size() > 0) {
			for (Users users : usersList) {

				ScreenerListVO screenerListVO = new ScreenerListVO();
				OverAllScreenerListVO overAllScreenerVO = new OverAllScreenerListVO();
				Users userName = userService.getUserByEmail(users.getUsername());
				List<Allocation> ids = allocationRepository.findAllocationOfScreeners(userName);
				List<Long> idAlloc = new ArrayList<Long>();
				if (ids.size() > 0) {
					for (Allocation allocation : ids) {
						idAlloc.add(allocation.getId());
					}
				
					// for pending jobs
					List<Allocation> pendingJobs = allocationRepository.findByAllocationIdsAndStatus(idAlloc,
							Status.NEW.name());
					screenerListVO.setJobsInQueue(pendingJobs.size());
				}
				int recruiterApprovedCount = profileRepository.findAllRecruiterApproved(userName.getId());
				int screenerApprovedCount = profileRepository.findAllScreenerApproved(userName.getId());
				
				screenerListVO.setScreenerName(userName.getEmail());
				if (screenerApprovedCount == 0) {
					screenerListVO.setSuccessRate("0%");
				} else {
					int successrate = (recruiterApprovedCount / screenerApprovedCount) * 100;

					screenerListVO.setSuccessRate(successrate + "%");
				}
				overAllScreenerVO.setScreenerListVO(screenerListVO);

				overAllScreenerListVO.add(overAllScreenerVO);

			}
		} else {

			result.setResult("FAILED");
			result.setErrors("No screeners under him");
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(overAllScreenerListVO, HttpStatus.OK);

	}

	/**
	 * offline/online screener availability
	 *
	 */

	@RequestMapping(value = "/admin/availability/{username:.+}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> availabilityForScreeners(@PathVariable String username){
		   
		Users user = userService.getUserByEmail(username);
		APIResult result = new APIResult();
		if (user != null) {

			// get online and offline screeners
			List<Socket> offlineScreeners = socketRepository.findOfflineScreeners(user.getEmail());
			List<Socket> onlineScreeners = socketRepository.findByUsername(user.getEmail());
			AvailabilityVO availabilityVO = new AvailabilityVO();

			// set online screener response
			if (onlineScreeners.size() > 0) {
				for (Socket socket : onlineScreeners) {

					availabilityVO.setAvailability(true);
					availabilityVO.setLastSeen(socket.getCreatedAt());
				}
			} else {
				// set offline screener response
				for (Socket socket : offlineScreeners) {

					// String socketUsername = socket.getUsername();

					availabilityVO.setAvailability(false);
					availabilityVO.setLastSeen(socket.getCreatedAt());

				}

			}

			return new ResponseEntity<>(availabilityVO, HttpStatus.OK);
		} else {
			result.setResult("FAILED");
			result.setErrors("No user with the email Exists");
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);

		}

	}

	/**
	 * screener List
	 *
	 */
	
	@RequestMapping(value = "/admin/screenerList/{username:.+}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> viewScreener(@PathVariable String username){
			
		LOGGER.debug("screener list is called..."+username);
		
		Users user = userService.getUserByEmail(username);
		
		APIResult result = new APIResult();
		if (user != null) {
			List<ViewScreenerVO> viewScreenerVOs = new ArrayList<ViewScreenerVO>();

			// get screenerApproved Profiles
			List<EntShortlist> shortlists = profileRepository.findScreenerStatus(user.getId());
			if (shortlists.size() > 0) {
				for (EntShortlist entShortlist : shortlists) {
					Person person = userService.findPersonDatasByProfileId(entShortlist.getProfileId());
					ViewScreenerVO viewScreenerVO = new ViewScreenerVO();
					viewScreenerVO.setCandidateName(
							person.getName().getGiven_name() + " " + person.getName().getFamily_name());
					viewScreenerVO.setJobId(entShortlist.getJobId());
					if (entShortlist.getScreenerResponse() == 0) {
						viewScreenerVO.setVerdict("pass");
					} else {
						viewScreenerVO.setVerdict("fail");
					}
					viewScreenerVO.setScreenedDate(entShortlist.getCreatedDateTime());
					viewScreenerVOs.add(viewScreenerVO);
				}
				return new ResponseEntity<>(viewScreenerVOs, HttpStatus.OK);
			} else {

				result.setResult("FAILED");
				result.setErrors("No screenering profiles Yet");
				return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);

			}
		} else {

			result.setResult("FAILED");
			result.setErrors("No user with the email Exists");
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);

		}
	}

	/**
	 * Search screener
	 *
	 */

	@RequestMapping(value = "/admin/screener", method = RequestMethod.GET, headers = "Accept=*/*", produces = "application/json")
	public ResponseEntity<?> searchScreener(@RequestParam(value = "jobId", required = false) String jobId)
			throws Exception {

		LOGGER.debug("search is called...");
		List<Person> profileIdList = new ArrayList<>();
		List<String> candidatenameList = new ArrayList<>();
		APIResult result = new APIResult();
		List<SearchResultVO> searchResultVO = new ArrayList<SearchResultVO>();

		// get screenerRejected profiles
		List<String> profileIds = profileRepository.findScreenerRejectedProfileId(jobId);
		if (profileIds.size() > 0) {
			profileIdList = service.findprofileIdsName(profileIds);
			SearchResultVO searchResult = new SearchResultVO();
			String candidatename;
			Person userName = null;
			for (Person person : profileIdList) {
				userName = service.findcandidateName(person.getName().getGiven_name());

				candidatename = userName.getName().getGiven_name();
				candidatenameList.add(candidatename);

			}

			searchResult.setUserList(candidatenameList);

			searchResultVO.add(searchResult);

			return new ResponseEntity<>(searchResultVO, HttpStatus.OK);
		} else {
			result.setResult("FAILED");
			result.setErrors("No screener rejected profiles");
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);

		}
	}

	/**
	 * overrideVerdictScreener
	 *
	 */

	@RequestMapping(value = "/admin/viewSreener/override", method = RequestMethod.GET, headers = "Accept=*/*", produces = "application/json")
	public ResponseEntity<?> overrideVerdictScreener(@RequestParam(value = "jobId", required = false) String jobId,
			@RequestParam(value = "candidateName", required = false) String candidateName) throws Exception {
		LOGGER.debug("overrideVerdictScreener is called...");
		APIResult result = new APIResult();

		List<OverrideVerdictVO> overrideVerdictVOs = new ArrayList<OverrideVerdictVO>();
		
		List<String> profileIds = profileRepository.findScreenerRejectedProfileId(jobId);
		List<Person> profileIdList = new ArrayList<>();
		if (profileIds.size() > 0) {
			profileIdList = service.findprofileName(profileIds, candidateName);
		} else {
			result.setResult("FAILED");
			result.setErrors("No ScreenerRejected Profiles");
			return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);

		}
		for (Person person : profileIdList) {
			List<EntShortlist> entShortlists=profileRepository.findByProfileIds(person.get_id());
		
				List<EntShortlist> screenerRecord = profileRepository.findScreeners(entShortlists.get(0).getShortlistId());
				OverrideVerdictVO overrideVerdictVO=new OverrideVerdictVO();
				overrideVerdictVO.setCandidateName(candidateName);
				
				overrideVerdictVO.setJobId(jobId);
				if(screenerRecord.get(0).equals(0)){
				overrideVerdictVO.setScreenerVerdict1("pass");
				
				}else{
					overrideVerdictVO.setScreenerVerdict1("fail");
					
				}
				if(screenerRecord.get(1).equals(0)){
					overrideVerdictVO.setScreenerVerdict2("pass");
					
					}else{
						overrideVerdictVO.setScreenerVerdict2("fail");
						
					}
				if(screenerRecord.get(2).equals(0)){
					overrideVerdictVO.setScreenerVerdict3("pass");
					
					}else{
						overrideVerdictVO.setScreenerVerdict3("fail");
						
					}
	
				overrideVerdictVO.setFinalStatus("fail");
				overrideVerdictVOs.add(overrideVerdictVO);
					
		}
		return new ResponseEntity<>(overrideVerdictVOs, HttpStatus.OK);

	}
	

	/**
	 * overrideVerdictScreener
	 *
	 */

	@RequestMapping(value = "/admin/override", method = RequestMethod.GET, headers = "Accept=*/*", produces = "application/json")
	@ResponseStatus(value = HttpStatus.OK)
	public void overrideScreener(@RequestParam(value = "jobId", required = false) String jobId,
			@RequestParam(value = "candidateName", required = false) String candidateName) throws Exception {

		List<EntShortlist> entShortlists = null;
		List<String> profileIds = profileRepository.findScreenerRejectedProfileId(jobId);
		List<Person> profileIdList = new ArrayList<>();
		if (profileIds.size() > 0) {
			profileIdList = service.findprofileName(profileIds, candidateName);
		}
		for (Person person : profileIdList) {
			entShortlists = profileRepository.findProfileId(jobId, person.get_id());
		}
		EntShortlist shortlist = new EntShortlist();
		for (EntShortlist entShortlist : entShortlists) {
			shortlist.setShortlistId(entShortlist.getShortlistId());
			shortlist.setJobId(entShortlist.getJobId());
			shortlist.setProfileId(entShortlist.getProfileId());
			int userId = 1;
			shortlist.setUserId(new Long(userId));
			shortlist.setCreatedDateTime(new Date());
			shortlist.setConsultant(entShortlist.getConsultant());
			shortlist.setStatus("SCREENER_APPROVED");
			shortlist.setScreenerResponse(0);
			service.createShortlist(shortlist);
		}

	}

}
