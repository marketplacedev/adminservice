package com.piqube.marketplace.util;


import com.piqube.marketplace.model.Authorities;
import com.piqube.marketplace.model.Notification;
import com.piqube.marketplace.model.Users;
import com.piqube.marketplace.vo.AllocationVO;
import com.piqube.marketplace.vo.UserVO;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdminServiceHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminServiceHelper.class);
    private static int workload = 12;
    Pattern letter = Pattern.compile("[a-zA-z]");
    Pattern digit = Pattern.compile("[0-9]");
    Pattern special = Pattern.compile ("[!@#$%&*()_+=|<>?{}\\[\\]~-]");
    Pattern eight = Pattern.compile (".{8}");

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
    public  boolean passwordMatcher(String password) {
        //Matcher hasLetter = letter.matcher(password);
        Object hasDigit=digit.matcher(password).find();
        Object hasSpecial=special.matcher(password).find();
        Object hasEight=eight.matcher(password).find();
        if(hasDigit.equals(true)   && hasSpecial.equals(true)
                &&  hasEight.equals(true)) {
            return true;
        }else{
            return false;
        }
    }
    public String getLoggedInUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username;
        if (principal instanceof UserDetails)
            username = ((UserDetails) principal).getUsername();
        else
            username = principal.toString();

        LOGGER.debug("Logged in user is:{}",username);
        return username;
    }


    public  List<String> validateAdminSignup(UserVO userVO) {
        List<String> result = new ArrayList<String>();
        Pattern phoneNumberPattern = Pattern.compile("^\\d{8,11}$");

        if (userVO.getEmail() == null)
            result.add("Please enter the Email");
        else {
            boolean validity = isValidEmailAddress(userVO.getEmail());
            if (!validity)
                result.add("Please enter a valid Email");
            if(userVO.getCompanyCategory()!=null){
            if(userVO.getCompanyCategory().equals("COMPANY")) {
                List<String> invalidEmailDomainList = Arrays.asList(userVO.getInvalidEmailDomains().split("\\s*,\\s*"));
                for (String invalidDomain : invalidEmailDomainList) {
                    if (userVO.getEmail().contains(invalidDomain))
                        result.add("Please enter a Corporate Email address");
                }
            }
        }else{
        	if (userVO.getCompanyCategory() == null)
                result.add("Please enter the CompanyCategory");
        }
        }
        
        if (userVO.getFirstName() == null)
            result.add("Please enter the FirstName");
        if (userVO.getLastName() == null)
            result.add("Please enter the LastName");
        if (userVO.getCompanyName() == null)
            result.add("Please enter the CompanyName");
        if (userVO.getCompanyType() == null)
            result.add("Please enter the CompanyType");
        if (userVO.getAuthority() == null)
            result.add("Please enter the correct authority name");
        if (userVO.getCompanySize() == null)
            result.add("Please enter the Company size");
        if (userVO.getCompanyLocation() == null || userVO.getCompanyLocation().isEmpty())
            result.add("Please enter the CompanyLocation");
        if (userVO.getPhoneNumber() == null)
            result.add("Please enter the  PhoneNumber");
        else {
            Pattern pattern = Pattern.compile("^\\d{8,11}$");
            if (!pattern.matcher(userVO.getPhoneNumber()).find()) {
                result.add("Please enter a valid PhoneNumber");
            }
        }
        return result;

    }

    public  List<String> validateSubUserSignup(UserVO userVO) {
        List<String> result = new ArrayList<String>();

        if (userVO.getPassword() == null)
            result.add("Please enter the Password");
        else if (!passwordMatcher(userVO.getPassword()))
            result.add("You password should be of at least 8 characters with one number and one special character\n");
        if (userVO.getFirstName() == null)
            result.add("Please enter the FirstName");
        if (userVO.getLastName() == null)
            result.add("Please enter the LastName");
        if (userVO.getPhoneNumber() == null)
            result.add("Please enter the  PhoneNumber");
        else {
            Pattern pattern = Pattern.compile("^\\d{8,11}$");
            if (!pattern.matcher(userVO.getPhoneNumber()).find()) {
                result.add("Please enter a valid PhoneNumber");
            }
        }
        return result;

    }
    
    public  List<String> validatePiqubeSignup(UserVO userVO) {
        List<String> result = new ArrayList<String>();
        if (userVO.getEmail() == null)
            result.add("Please enter the Email");
        if (userVO.getFirstName() == null)
            result.add("Please enter the FirstName");
        if (userVO.getLastName() == null)
            result.add("Please enter the LastName");
        if (userVO.getAuthority() == null)
            result.add("Please enter the Authority");
        if (userVO.getPhoneNumber() == null)
            result.add("Please enter the  PhoneNumber");
        else {
            Pattern pattern = Pattern.compile("^\\d{8,11}$");
            if (!pattern.matcher(userVO.getPhoneNumber()).find()) {
                result.add("Please enter a valid PhoneNumber");
            }
        }
        return result;

    }
    public  List<String> validateAdminSubUserSignup(UserVO userVO) {
        List<String> result = new ArrayList<String>();
        if (userVO.getEmail() == null)
            result.add("Please enter the Email");
        
        if (userVO.getFirstName() == null)
            result.add("Please enter the FirstName");
        if (userVO.getLastName() == null)
            result.add("Please enter the LastName");
        if (userVO.getCompanyName() == null)
            result.add("Please enter the CompanyName");
        if (userVO.getAuthority() == null)
            result.add("Please enter the Authority");
        if (userVO.getCompanyLocation() == null || userVO.getCompanyLocation().isEmpty())
            result.add("Please enter the CompanyLocation");
        if (userVO.getPhoneNumber() == null)
            result.add("Please enter the  PhoneNumber");
        else {
            Pattern pattern = Pattern.compile("^\\d{8,11}$");
            if (!pattern.matcher(userVO.getPhoneNumber()).find()) {
                result.add("Please enter a valid PhoneNumber");
            }
        }
        return result;

    }
    public  List<String> validateScreenerAllocation(AllocationVO allocationVO) {
        List<String> result = new ArrayList<String>();
        if (allocationVO.getEmail() == null)
            result.add("Please provide the UserId");
        if (allocationVO.getJobId() == null)
            result.add("Please provide the JobId");
        if (allocationVO.getProfileId() == null)
            result.add("Please provide the ProfileId");
    
        return result;

    }
    
    

    public Map<String, Integer> setPreferences(Map<String, String> preferences) {
        Map<String, Integer> result = new HashMap<String, Integer>();
        Iterator iter = preferences.entrySet().iterator();

        while(iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            result.put("IND " + entry.getValue(), Integer.valueOf( entry.getKey().toString()));
        }
        return result;
    }
   
	 public List<String> getAllocationStatuses(){
	        List<String> statusList=new ArrayList<String>();
	        statusList.add("NEW");
	        statusList.add("ACCEPTED");
	        
	        return statusList;
	    }
	    
}
